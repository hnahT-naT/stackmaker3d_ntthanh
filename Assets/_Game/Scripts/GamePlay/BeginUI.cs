using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BeginUI : MonoBehaviour
{
    [SerializeField] private Button newGameButton;
    [SerializeField] private Button loadGameButton;
    [SerializeField] private Image warning;
    [SerializeField] private Player player;
    private void Start()
    {
        newGameButton.onClick.AddListener(NewLevel);
        loadGameButton.onClick.AddListener(LoadGame);
    }

    private void Update()
    {
        int playedLevel = PlayerPrefs.GetInt("level");
        //Debug.Log(playedLevel);
        if (playedLevel == 0)
        {
            loadGameButton.interactable = false;
        }
        else
        {
            loadGameButton.interactable = true;
        }
    }
    private void NewLevel()
    {
        PlayerPrefs.SetInt("level", 0);
        //player.LoadLevel(0);
    }
    private void LoadGame()
    {
        int playedLevel = PlayerPrefs.GetInt("level");
       // Debug.Log(playedLevel);
        if(playedLevel == 0)
        {
            warning.gameObject.SetActive(true);
            Invoke(nameof(shutDownWarning), 5f);
        }
        else
        {
            //player.LoadLevel(playedLevel);
        }    
    }
    private void shutDownWarning()
    {
        warning.gameObject.SetActive(false);
    }
}
