using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    [SerializeField] private float speed = 1f;
    [SerializeField] private GameObject stepBrick;  
    [SerializeField] private Transform playerBody;
    [SerializeField] private Animator anim;
    [SerializeField] private LayerMask stepBrickLayer;
    [SerializeField] private LayerMask brickStreet;
    [SerializeField] private GameObject defeatedUI;
    [SerializeField] private GameObject victoryUI;
    [SerializeField] private GameObject beginUI;
    [SerializeField] private Text brickText;
    [SerializeField] private List<GameObject> levelList;

    private Vector3 startMousePoint, endMousePoint, direct;
    private Vector3 finishPoint;
    private string currentAnimName;
    private bool isMoving;
    private int brickCount;
    private Transform startPoint;
    private GameObject currentLevel;
  
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        // khi khong trong state gameplay, khong lam gi khac
        if (!GameManager.Instance.IsState(GameState.Gameplay))
        {
            return;
        }
        // ************************ find finishPoint*********************************
        // khi co direct, tim finishPoint, va khong lam gi khac
        if (direct != Vector3.zero)
        {
            FindFinishPoint();
            return;
        }
        //************************** moving ******************************************
        if (Vector3.Distance(transform.position, finishPoint) > 0.1f)
        {
            ChangeAnim("idle");
            isMoving = true;
            transform.position = Vector3.MoveTowards(transform.position, finishPoint, speed * Time.deltaTime);
            if (Vector3.Distance(transform.position, finishPoint) < 0.1f)
            {
                transform.position = finishPoint;
                isMoving = false;
            }
        }
        // dang di chuyen, khong lam gi khac
        if (isMoving)
        {
            return;
        }
        // ************************check moving direct*****************************
        // check vi tri an chuot
        if (Input.GetMouseButtonDown(0))
        {
            startMousePoint = Input.mousePosition;
            direct = Vector3.zero;
            // check vi tri nha chuot
            Invoke(nameof(GetEndMousePoint), 0.1f);
        }
    }
    // reset thong so 
    public void OnInit()
    {
        // change anim
        ChangeAnim("idle");
        // set cac bien dieu kien di chuyen ve 0
        isMoving = false;
        // set so brickCount = 1
        brickCount = 1;
        // set up vi tri Player
        transform.position += Vector3.up*3f;
        // set vi tri ban dau cua finishPoint
        finishPoint = transform.position;
        // set up trang thai dau tien cua Player (co 1 vien gach ngay duoi chan)
        playerBody.position = transform.position;
        Instantiate(stepBrick, transform.position - Vector3.up * 0.5f, Quaternion.Euler(-90, 0, -180), transform);
    }
    // that bai
    private void OnDefeated()
    {
        LevelManager.Instance.OnLose();
    }
    // chien thang
    public void OnVictory()
    {
        ClearBrick();
        ChangeAnim("victory");
        brickText.text = brickCount.ToString();
        LevelManager.Instance.OnFinish();
    }
  
    // them gach
    public void AddBrick()
    {
        brickCount++;
        ChangeAnim("changebrick");
        // di chuyen body len tren (y+0.5)
        playerBody.position += Vector3.up * 0.5f;
        // tao stepBrick clone
        Instantiate(stepBrick, playerBody.position - Vector3.up * 0.5f, Quaternion.Euler(new Vector3(-90, 0, -180)), transform);
    }
    //xoa stepBrick
    public void RemoveBrick()
    {
        brickCount--;
        ChangeAnim("changebrick");
        // xoa brick
        // raycast kiem tra co stepBrick duoi chan ko 
        RaycastHit hit1;
        if (Physics.Raycast(playerBody.position, Vector3.down, out hit1, Mathf.Infinity, stepBrickLayer))
        {
            // xoa stepBrick duoi chan
            Destroy(hit1.collider.gameObject);
            // di chuyen body xuong duoi (y-0.5)
            playerBody.position += Vector3.down * 0.5f;
        }
        else
        {
            OnDefeated();
        }
    }
    // xoa all stepBrick
    private void ClearBrick()
    {
        RaycastHit hit;
        while(Physics.Raycast(playerBody.position, Vector3.down, out hit, Mathf.Infinity, stepBrickLayer))
        {
            // xoa stepBrick duoi chan
           // Debug.Log(hit.collider.name);
            Destroy(hit.collider.gameObject);
            playerBody.position += Vector3.down * 0.5f;
           // Debug.LogWarning(playerBody.position);
        }

    }
    // lay vi tri nha chuot
    private void GetEndMousePoint()
    {
        endMousePoint = Input.mousePosition;
        direct = ConvertDirect(endMousePoint - startMousePoint);
        //Debug.Log(direct);
    }
    // chuyen huong di chuot thanh huong di chuyen cua nhan vat
    private Vector3 ConvertDirect(Vector3 mouseDirect)
    {
        // lay Abs cua toa do x ,y cua mouseDirect
        float x = Mathf.Abs(mouseDirect.x), y = Mathf.Abs(mouseDirect.y);

        // check neu doan di chuot qua nho, return zero
        if (x < 100f && y < 100f)
        {
            return Vector3.zero;
        }

        // check neu di chuot cheo, return zero
        if (Mathf.Abs(x - y) < x && Mathf.Abs(x - y) < y)
        {
            return Vector3.zero;
        }

        // check di chuyen theo truc y
        if (y >= x)
        {
            if (mouseDirect.y > 0) return Vector3.forward;
            else return Vector3.back;
        }

        // check di chuyen theo truc x 
        else
        {
            if (mouseDirect.x > 0) return Vector3.right;
            else return Vector3.left;
        }
    }
    // raycast tim FinishPoint
    private void FindFinishPoint()
    {
        //raycast 
        if (Physics.Raycast(finishPoint + direct, Vector3.down, brickStreet))
        {
            finishPoint += direct;
        }
        else
        {
            Debug.DrawRay(finishPoint + direct, Vector3.down, Color.blue, 10f);
            // quay huong nhan vat 
            if (Vector3.Distance(transform.position, finishPoint) > 0.1f)
            {
                if (direct.x == 0)
                {
                    playerBody.rotation = Quaternion.Euler(new Vector3(0, direct.z == 1 ? 0 : 180, 0));
                }
                else
                {
                    playerBody.rotation = Quaternion.Euler(new Vector3(0, direct.x == 1 ? 90 : -90, 0));
                }
            }
            // reset direct
            direct = Vector3.zero;
        }
    }
    // change anim
    private void ChangeAnim(string animName)
    {
        if (currentAnimName != animName)
        {
            anim.ResetTrigger(animName);
            currentAnimName = animName;
            anim.SetTrigger(currentAnimName);
        }
    }

}


/*  
 *  
 *  
    // chay Victory UI
    private void VictoryUI()
    {
        victoryUI.SetActive(true);
    }
    // choi lai man vua roi 
    public void PlayAgain()
    {
        LoadLevel(currentLevelIndex);  
    }
    // choi man tiep theo
    public void PlayNextLevel()
    {
        PlayerPrefs.SetInt("level", currentLevelIndex+1);
        LoadLevel(currentLevelIndex+1);
    }
    // chon man choi
    public void LoadLevel(int levelIndex)
    {
        if(currentLevel!=null)
        {
            Destroy(currentLevel);
        }
        currentLevelIndex = levelIndex;
        currentLevel =  Instantiate(levelList[levelIndex]);
        OnInit();
    }

 */