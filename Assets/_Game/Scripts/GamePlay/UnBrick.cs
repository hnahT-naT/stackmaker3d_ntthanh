using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnBrick : MonoBehaviour
{
    [SerializeField] private GameObject stepBrick;
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            other.GetComponent<Player>().RemoveBrick();
            if(GameManager.Instance.IsState(GameState.Gameplay))
            {
                transform.position -= Vector3.up * 0.5f;
                Instantiate(stepBrick, transform.position+Vector3.up*2.5f,Quaternion.Euler(-90,0,-180), transform);
            }
            
        }
    }
}
