using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Brick : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        // neu va cham vs Player
        if(other.tag == "Player")
        {
            //goi ham AddBrick cua Player
            other.GetComponent<Player>().AddBrick();
            // deactive TopBrick va cham vs Player
            Destroy(gameObject);
            //gameObject.SetActive(false);
        }
    }
}
