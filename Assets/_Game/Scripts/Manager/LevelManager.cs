using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : Singleton<LevelManager>
{
    public List<Level> levels = new List<Level>();
    public Player player;
    private Level currentLevel;
    public int currentLevelIndex = 1;

    private void Start()
    {
        currentLevelIndex = 1;
        UIManager.Instance.OpenMainMenuUI();
    }

    public void LoadLevel()
    {
        LoadLevel(currentLevelIndex);
    }

    public void NextLevel()
    {
        currentLevelIndex ++;
        LoadLevel();
    }    

    // load level
    public void LoadLevel(int indexLevel)
    {
        if (currentLevel != null)
        {
            Destroy(currentLevel.gameObject);
        }
        currentLevel = Instantiate(levels[indexLevel - 1]);
        OnInit();
    }
    // reset thong so
    public void OnInit()
    {
        player.transform.position = currentLevel.startPoint.position;
        player.OnInit();
    }

    // bat dau game
    public void OnStart()
    {
        GameManager.Instance.ChangeState(GameState.Gameplay);
    }
    // ket thuc game
    public void OnFinish()
    {
        Invoke(nameof(UIManager.Instance.OpenFinishUI),2f);
        GameManager.Instance.ChangeState(GameState.Finish);
    }

    public void OnLose()
    {
        UIManager.Instance.OpenLoseUI();
        GameManager.Instance.ChangeState(GameState.Finish);
    }
}
