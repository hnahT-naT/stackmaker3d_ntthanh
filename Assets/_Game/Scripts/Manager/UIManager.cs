using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : Singleton<UIManager>
{
    public GameObject mainmenuUI;
    public GameObject finishUI;
    public GameObject loseUI;

    public void OpenMainMenuUI()
    {
        mainmenuUI.SetActive(true);
        finishUI.SetActive(false);
        loseUI.SetActive(false);
    }
    
    public void OpenFinishUI()
    {
        finishUI.SetActive(true);
    }
    public void OpenLoseUI()
    {
        loseUI.SetActive(true);
    }

    // bat dau choi 
    public void NewGameButton()
    {
        mainmenuUI.SetActive(false);
        LevelManager.Instance.LoadLevel();
        LevelManager.Instance.OnStart();
    }
    // choi lai man 
    public void RetryButton()
    {
        finishUI.SetActive(false);
        loseUI.SetActive(false);
        LevelManager.Instance.LoadLevel();
        LevelManager.Instance.OnStart();
    }

    // choi man moi
    public void NextLevelButton()
    {
        finishUI.SetActive(false);
        LevelManager.Instance.NextLevel();
        LevelManager.Instance.OnStart();
    }
}
